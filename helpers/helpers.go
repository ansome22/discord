package helpers

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"
)

//GetURLFile get a file from the internet either JSON or XML
func GetURLFile(filetype string, url string, target interface{}) error {
	// to get json and xml files or other online files
	var myhttpClient = &http.Client{Timeout: time.Second * 15}

	r, err := myhttpClient.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Body.Close()
	switch filetype {
	case "json":
		return json.NewDecoder(r.Body).Decode(target)
	case "xml":
		return xml.NewDecoder(r.Body).Decode(target)
	}
	return nil
}

//GetLocalFile get a file locally either JSON or XML
func GetLocalFile(filetype string, url string, target interface{}) error {
	r, err := ioutil.ReadFile(url)
	if err != nil {
		log.Fatal(err)
	}
	switch filetype {
	case "json":
		return json.Unmarshal(r, &target)
	case "xml":
		return xml.Unmarshal(r, &target)
	}
	return nil
}

//WriteLocalFile creates/modifies a file locally either JSON or XML
func WriteLocalFile(filetype string, url string, target interface{}) {
	switch filetype {
	case "json":
		json, _ := json.Marshal(target)
		err := ioutil.WriteFile(url, json, 0644)
		if err != nil {
			log.Fatal(err)
		}
	case "xml":
		xml, _ := xml.Marshal(target)
		err := ioutil.WriteFile(url, xml, 0644)
		if err != nil {
			log.Fatal(err)
		}
	}
}

//Convertfloat to string
func Convertfloat(float float64) string {
	return strconv.FormatFloat(float, 'f', -1, 64)
}

//Convertint to string
func Convertint(i int) string {
	return strconv.Itoa(i)
}

//Printstruc prints a structure varible
func Printstruc(arc interface{}) {
	fmt.Println("\nStructure:")

	data, err := json.Marshal(arc)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", data)
}

//Stringstruc Converts structs to JSON.
func Stringstruc(arc interface{}) string {
	data, err := json.Marshal(arc)
	if err != nil {
		log.Fatal(err)
	}
	return string(data)
}

//Arraytostring coverts array to a string
func Arraytostring(array []string) string {
	return strings.Join(array, ", ")
}

//Arraytostringnewline coverts array to a string with new line for each item
func Arraytostringnewline(array []string) string {
	return strings.Join(array, "\n")
}

//Arraytostringnewlinenumbers coverts array to a string with a number for each item
func Arraytostringnewlinenumbers(array []string) string {
	varstring := ""

	for index, item := range array {
		varstring = varstring + Convertint(index+1) + ". " + item + "\n"
	}

	return varstring
}

// GetFields Used to get fields
func GetFields(i interface{}) (res []string) {
	v := reflect.ValueOf(i)
	for j := 0; j < v.NumField(); j++ {
		res = append(res, v.Field(j).String())
	}
	return
}

//Pickrandom selects an item from array randomly
func Pickrandom(array []string) string {
	return array[rand.Intn(len(array))]
}
