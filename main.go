package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"./audio"
	"./fun"
	"./helpers"
	"./structures"
	"github.com/bwmarrin/discordgo"
)

// Variables used for command line parameters
var (
	Token string
)

func init() {
	flag.StringVar(&Token, "t", "", "Bot Token")
	flag.Parse()
}

func main() {
	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}
	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)
	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}
	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
	// Cleanly close down the Discord session.
	dg.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}
	uncapmessage := strings.ToLower(m.Content)
	//begin reading user message
	go readwholemesssage(uncapmessage, s, m)
	//split string
	// lowercase array
	uncapmessagearray := strings.Split(uncapmessage, " ")
	// uppercase array
	message := strings.Split(m.Content, " ")
	go readEarchPartOfTheMesssage(message, uncapmessagearray, s, m)

}

func readwholemesssage(message string, s *discordgo.Session, m *discordgo.MessageCreate) {
	switch message {
	// If the message is "ping" reply with "Pong!"
	case "ping":
		s.ChannelMessageSend(m.ChannelID, "Pong!")
	// If the message is "pong" reply with "Ping!"
	case "pong":
		s.ChannelMessageSend(m.ChannelID, "Ping!")
	// Bot acknowledges the person is a whale
	case "i'm a whale":
		s.ChannelMessageSend(m.ChannelID, "He's a Whale!")
	//Force bot to know who it belongs to
	case "who is your master?":
		s.ChannelMessageSend(m.ChannelID, "Ansome!")
	//send user list of commands
	case "bot help":
		embed := structures.NewEmbed().
			SetTitle("Available Commands:").
			AddField("Pokemon", "pokesearch help").
			AddField("Black Desert Online", "bdo help").
			AddField("Fortnite", "fortsearch help").
			AddField("Cryptosearch", "cryptosearch help").
			AddField("Info", "bot info").
			AddField("Music", "music help").
			SetColor(0x00ff00).MessageEmbed
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
	case "bot info":
		embed := structures.NewEmbed().
			SetTitle("Bot Info").
			SetDescription("A discord bot made by Ansome in go").
			SetURL("https://gitlab.com/ansome22/discord").
			SetColor(0x00ff00).MessageEmbed
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
		//search if in meme
		fun.MemeInit(message, s, m)
		//search for sounds
		audio.SoundInit(message)
		//search for animals
		fun.AnimalInit(message)
	}
}

func readEarchPartOfTheMesssage(message []string, uncapmessage []string, s *discordgo.Session, m *discordgo.MessageCreate) {
	//if message is larger than 2 words
	if len(uncapmessage) >= 2 {
		switch uncapmessage[0] {
		case "urban":
			searchvar := fun.UrbandictionaryGet(strings.Join(uncapmessage[1:], ""))
			embed := structures.NewEmbed().
				SetTitle(searchvar.List[0].Word).
				AddField("Rank", searchvar.List[0].Definition).
				AddField("Example", searchvar.List[0].Example).
				AddField("ThumbsUp", helpers.Convertint(searchvar.List[0].ThumbsUp)).
				SetURL(searchvar.List[0].Permalink).
				SetColor(0x00ff00).MessageEmbed
			s.ChannelMessageSendEmbed(m.ChannelID, embed)
		// My Anime List
		case "mal":
			searchvar := new(structures.Mal)
			switch uncapmessage[1] {
			case "search":
				helpers.GetURLFile("xml", "", searchvar)
				s.ChannelMessageSend(m.ChannelID, searchvar.Name)

			case "help":
				s.ChannelMessageSend(m.ChannelID, "Available Commands: \nMAL search [anime/manga Name]")
			}
		// Search Pokmon
		case "pokesearch":
			fun.PokemonInit(uncapmessage, message, s, m)
		// Search data for cryptocurrency
		case "cryptosearch":
			searchvar := structures.Cyrpto{}
			helpers.GetURLFile("json", "https://coinbin.org/"+message[1], &searchvar)
			embed := structures.NewEmbed().
				SetTitle(searchvar.Coin.Name).
				AddField("Rank", helpers.Convertint(searchvar.Coin.Rank)).
				AddField("Price (USD)", helpers.Convertfloat(searchvar.Coin.Usd)).
				SetColor(0x00ff00).MessageEmbed
			s.ChannelMessageSendEmbed(m.ChannelID, embed)
		//Balck Desert Online
		case "bdo":
			switch uncapmessage[1] {
			case "map":
				embed := structures.NewEmbed().
					SetTitle("Black Desert Online Map").
					SetDescription("Site that contains a Interactive for Black Desert Online").
					SetImage("https://i.imgur.com/pwonU0W.jpg").
					SetURL("http://www.somethinglovely.net/bdo/").
					SetColor(0x820000).MessageEmbed
				s.ChannelMessageSendEmbed(m.ChannelID, embed)
			case "whale":
				embed := structures.NewEmbed().
					SetTitle("Black Desert Online Whale Map").
					SetDescription("Image of whale locations").
					SetImage("https://i.imgur.com/vADnXX5.jpg").
					SetColor(0x820000).MessageEmbed
				s.ChannelMessageSendEmbed(m.ChannelID, embed)
			case "analytics":
				embed := structures.NewEmbed().
					SetTitle("Black Desert Online Analytics").
					SetDescription("Site that....").
					SetImage("http://blackdesertanalytics.com/images/indexbanner.jpg").
					SetURL("http://blackdesertanalytics.com/").
					SetColor(0x820000).MessageEmbed
				s.ChannelMessageSendEmbed(m.ChannelID, embed)
			case "help":
				embed := structures.NewEmbed().
					AddField("bdo map", "Map of Black Desert Online").
					AddField("bdo whale", "Whale Map for Black Desert Online").
					AddField("bdo analytics", "Analytics on Black Desert Online").
					SetColor(0x820000).MessageEmbed
				s.ChannelMessageSendEmbed(m.ChannelID, embed)
			}
			//go to Music file if message contains music
		case "music":
			audio.MusicInit(uncapmessage, message, s, m)
		}
	}
}
