package audio

import (
	"errors"
	"fmt"
	"io"
	"log"
	"math/rand"
	"time"

	"../helpers"
	"../structures"

	"github.com/bwmarrin/discordgo"
	"github.com/jonas747/dca"
	"github.com/rylio/ytdl"
)

type musicplayer struct {
	State     string
	Playing   string
	channelid string
	Song      []string
}

// Music
var musicdata = musicplayer{}
var vc = discordgo.VoiceConnection{}

//Youtube

//MusicInit to start up music
func MusicInit(uncapmessage []string, message []string, s *discordgo.Session, m *discordgo.MessageCreate) {
	switch uncapmessage[1] {
	case "list":
		musiclist(s, m)
	case "shuffle":
		musicshuffle(s, m)
	case "leave":
		if musicdata.State == "Playing" {
			vc.Disconnect()
		}
	case "help":
		embed := structures.NewEmbed().
			SetTitle("Music Help").
			SetDescription("Curently only supports youtube").
			AddField("music [url]", "Add song to queue").
			AddField("music list", "List music").
			AddField("music shuffle", "Shuffle playlist").MessageEmbed
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
	default:
		//make sure people have sent a possible url
		if len(message) >= 2 {
			switch musicdata.State {
			case "Playing":
				videoInfo := getvideoinfo(message[1])
				// if no error add song
				if videoInfo.Title != "" {
					musicdata.Song = append(musicdata.Song, message[1])
					embed := structures.NewEmbed().
						SetTitle("Added: "+videoInfo.Title).
						AddField("Author:", videoInfo.Author).
						AddField("Duration: ", videoInfo.Duration.String()).
						SetThumbnail(videoInfo.GetThumbnailURL("sddefault").String()).
						SetURL(message[1]).
						SetColor(0x820000).MessageEmbed
					s.ChannelMessageSendEmbed(m.ChannelID, embed)
				}
				//if first time
			default:
				musicdata.State = "Playing"
				vc, err := joinUserVoiceChannel(s, m.Author.ID)
				if err != nil {
					musicdata.State = ""
					return
				}
				// Change these accordingly
				options := dca.StdEncodeOptions
				options.RawOutput = true
				options.Bitrate = 70
				options.Application = "audio"
				options.Threads = 2
				options.PacketLoss = 0
				go music(s, m, vc, options, message[1])
			}
		}
	}
}

func findUserVoiceState(session *discordgo.Session, userid string) (*discordgo.VoiceState, error) {
	for _, guild := range session.State.Guilds {
		for _, vs := range guild.VoiceStates {
			if vs.UserID == userid {
				return vs, nil
			}
		}
	}
	return nil, errors.New("Could not find user's voice state")
}

func joinUserVoiceChannel(session *discordgo.Session, userID string) (*discordgo.VoiceConnection, error) {
	// Find a user's current voice channel
	vs, err := findUserVoiceState(session, userID)
	if err != nil {
		return nil, err
	}

	// Join the user's channel and start unmuted and deafened.
	return session.ChannelVoiceJoin(vs.GuildID, vs.ChannelID, false, true)
}

func getvideoinfo(message string) *ytdl.VideoInfo {
	videoInfo, err := ytdl.GetVideoInfo(message)
	if err != nil {
		log.Println("GetVideoInfo:")
		log.Fatal(err)
		return nil
	}
	return videoInfo
}

func music(s *discordgo.Session, m *discordgo.MessageCreate, vc *discordgo.VoiceConnection, options *dca.EncodeOptions, message string) {
	videoInfo := getvideoinfo(message)
	//library does not check url so if no title then not a valid video
	if videoInfo.Title == "" {
		//if only one song
		if len(musicdata.Song) < 2 {
			musicdata.State = ""
			vc.Disconnect()
		}
		embed := structures.NewEmbed().
			SetTitle("Failed to add song:").
			AddField("Input:", message).
			SetColor(0x820000).MessageEmbed
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
		return
	}
	embed := structures.NewEmbed().
		SetTitle("Playing: "+videoInfo.Title).
		AddField("Author:", videoInfo.Author).
		AddField("Duration: ", videoInfo.Duration.String()).
		SetThumbnail(videoInfo.GetThumbnailURL("sddefault").String()).
		SetURL(message).
		SetColor(0x820000).MessageEmbed
	s.ChannelMessageSendEmbed(m.ChannelID, embed)

	format := videoInfo.Formats.Extremes(ytdl.FormatAudioBitrateKey, true)[0]
	downloadURL, err := videoInfo.GetDownloadURL(format)
	if err != nil {
		log.Println("GetDownloadURL:")
		log.Fatal(err)
		musicdata.State = ""
		return
	}

	encodingSession, err := dca.EncodeFile(downloadURL.String(), options)
	if err != nil {
		log.Fatal(err)
		musicdata.State = ""
		return
	}
	//passed the test so can say we will play song
	musicdata.Song = append(musicdata.Song, message)
	defer encodingSession.Cleanup()
	done := make(chan error)
	dca.NewStream(encodingSession, vc, done)
	err = <-done
	//if error unsure if to leave or to try next song in queue
	if err != nil && err != io.EOF {
		log.Fatal("An error occured", err)
		return
	}
	// Clean up incase something happened and ffmpeg is still running
	encodingSession.Truncate()
	//if there are more songs play next song
	//else change state to not playing and leave
	switch len(musicdata.Song) {
	case 1:
		musicdata.Song = nil
		musicdata.State = ""
		vc.Disconnect()
		return
	default:
		//song done so remove
		musicdata.Song = musicdata.Song[1:]
		//play next song
		music(s, m, vc, options, musicdata.Song[0])
	}
}

//shows stats of video (decoding info and session)
func musicdebug(s *discordgo.Session, m *discordgo.MessageCreate, vc *discordgo.VoiceConnection, options *dca.EncodeOptions, message string) {
	//get video info (title,thumbnail etc)
	videoInfo, err := ytdl.GetVideoInfo(message)
	if err != nil {
		log.Println("GetVideoInfo:")
		log.Fatal(err)
		musicdata.State = ""
		return
	}

	format := videoInfo.Formats.Extremes(ytdl.FormatAudioBitrateKey, true)[0]
	downloadURL, err := videoInfo.GetDownloadURL(format)
	if err != nil {
		log.Println("GetDownloadURL:")
		log.Fatal(err)
		musicdata.State = ""
		return
	}

	encodingSession, err := dca.EncodeFile(downloadURL.String(), options)
	if err != nil {
		log.Fatal(err)
		musicdata.State = ""
		return
	}
	//passed the test so can say we will play song
	musicdata.Song = append(musicdata.Song, message)
	defer encodingSession.Cleanup()
	done := make(chan error)
	stream := dca.NewStream(encodingSession, vc, done)
	ticker := time.NewTicker(time.Second)
	for {
		select {
		case err := <-done:
			if err != nil && err != io.EOF {
				log.Fatal("An error occured", err)
			}
			fmt.Println("Song Done")
			fmt.Println(musicdata.Song)
			musicdata.Song = musicdata.Song[1:]
			fmt.Println(musicdata.Song)
			// Clean up incase something happened and ffmpeg is still running
			encodingSession.Truncate()
			//can only pop item when there is more than one
			//if there are more songs play next song
			if len(musicdata.Song) >= 2 {
				music(s, m, vc, options, musicdata.Song[0])
			}
			musicdata.Song = nil
			musicdata.State = ""
			return
		case <-ticker.C:
			stats := encodingSession.Stats()
			playbackPosition := stream.PlaybackPosition()
			fmt.Printf("Playback: %10s, Transcode Stats: Time: %5s, Size: %5dkB, Bitrate: %6.2fkB, Speed: %5.1fx\r", playbackPosition, stats.Duration.String(), stats.Size, stats.Bitrate, stats.Speed)
		}
	}
}

func musicshuffle(s *discordgo.Session, m *discordgo.MessageCreate) {
	for i := len(musicdata.Song) - 1; i > 0; i-- {
		j := rand.Intn(i + 1)
		musicdata.Song[i], musicdata.Song[j] = musicdata.Song[j], musicdata.Song[i]
	}
	embed := structures.NewEmbed().
		SetTitle("New list of Songs").
		AddField("Songs: ", helpers.Arraytostringnewline(musicdata.Song)).
		SetColor(0x820000).MessageEmbed
	s.ChannelMessageSendEmbed(m.ChannelID, embed)
}

func musiclist(s *discordgo.Session, m *discordgo.MessageCreate) {
	if musicdata.Song != nil {
		list := []string{}
		var total time.Duration
		for _, item := range musicdata.Song {
			videoinfo := getvideoinfo(item)
			list = append(list, videoinfo.Title)
			total = total + videoinfo.Duration
		}
		embed := structures.NewEmbed().
			SetTitle("List of Songs").
			AddField("Songs: ", helpers.Arraytostringnewlinenumbers(list)).
			AddField("Total Time", total.String()).
			SetColor(0x820000).MessageEmbed
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
	} else {
		embed := structures.NewEmbed().
			SetTitle("List of Songs").
			AddField("Songs: ", "No Songs have been added").
			SetColor(0x820000).MessageEmbed
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
	}
}
