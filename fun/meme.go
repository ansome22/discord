package fun

import (
	"../helpers"
	"github.com/bwmarrin/discordgo"
)

type memes struct {
	Jojo     []string `json:"Jojo"`
	Lewd     []string `json:"lewd"`
	Sumfuk   []string `json:"sumfuk"`
	Thumbsup []string `json:"thumbsup"`
}

//MemeInit search message for meme related items
func MemeInit(message string, s *discordgo.Session, m *discordgo.MessageCreate) {
	// Memes
	var memesvar = memes{}
	helpers.GetLocalFile("json", "./json/meme.json", &memesvar)
	switch message {
	case "jojo":
		picked := helpers.Pickrandom(memesvar.Jojo)
		s.ChannelMessageSend(m.ChannelID, picked)
	case "lewd":
		picked := helpers.Pickrandom(memesvar.Lewd)
		s.ChannelMessageSend(m.ChannelID, picked)
	case "sumfuk":
		picked := helpers.Pickrandom(memesvar.Sumfuk)
		s.ChannelMessageSend(m.ChannelID, picked)
	case "thumbsup":
		picked := helpers.Pickrandom(memesvar.Thumbsup)
		s.ChannelMessageSend(m.ChannelID, picked)
	}
}

func memeJojo() string {
	// Memes
	var memesvar = memes{}
	return helpers.Pickrandom(memesvar.Jojo)
}

func memeLewd() string {
	// Memes
	var memesvar = memes{}
	return helpers.Pickrandom(memesvar.Lewd)
}

func memeSumfuk() string {
	// Memes
	var memesvar = memes{}
	return helpers.Pickrandom(memesvar.Sumfuk)
}

func memeThumbsup() string {
	// Memes
	var memesvar = memes{}
	return helpers.Pickrandom(memesvar.Thumbsup)
}
