package fun

import (
	"../helpers"
	"../structures"
	"github.com/bwmarrin/discordgo"
)

//Pokemon structure
type Pokemon struct {
	Forms []struct {
		URL  string `json:"url"`
		Name string `json:"name"`
	} `json:"forms"`
	Abilities []struct {
		Slot     int  `json:"slot"`
		IsHidden bool `json:"is_hidden"`
		Ability  struct {
			URL  string `json:"url"`
			Name string `json:"name"`
		} `json:"ability"`
	} `json:"abilities"`
	Stats []struct {
		Stat struct {
			URL  string `json:"url"`
			Name string `json:"name"`
		} `json:"stat"`
		Effort   int `json:"effort"`
		BaseStat int `json:"base_stat"`
	} `json:"stats"`
	Name   string `json:"name"`
	Weight int    `json:"weight"`
	Moves  []struct {
		VersionGroupDetails []struct {
			MoveLearnMethod struct {
				URL  string `json:"url"`
				Name string `json:"name"`
			} `json:"move_learn_method"`
			LevelLearnedAt int `json:"level_learned_at"`
			VersionGroup   struct {
				URL  string `json:"url"`
				Name string `json:"name"`
			} `json:"version_group"`
		} `json:"version_group_details"`
		Move struct {
			URL  string `json:"url"`
			Name string `json:"name"`
		} `json:"move"`
	} `json:"moves"`
	Sprites struct {
		BackFemale       interface{} `json:"back_female"`
		BackShinyFemale  interface{} `json:"back_shiny_female"`
		BackDefault      string      `json:"back_default"`
		FrontFemale      interface{} `json:"front_female"`
		FrontShinyFemale interface{} `json:"front_shiny_female"`
		BackShiny        string      `json:"back_shiny"`
		FrontDefault     string      `json:"front_default"`
		FrontShiny       string      `json:"front_shiny"`
	} `json:"sprites"`
	HeldItems              []interface{} `json:"held_items"`
	LocationAreaEncounters string        `json:"location_area_encounters"`
	Height                 int           `json:"height"`
	IsDefault              bool          `json:"is_default"`
	Species                struct {
		URL  string `json:"url"`
		Name string `json:"name"`
	} `json:"species"`
	ID          int `json:"id"`
	Order       int `json:"order"`
	GameIndices []struct {
		Version struct {
			URL  string `json:"url"`
			Name string `json:"name"`
		} `json:"version"`
		GameIndex int `json:"game_index"`
	} `json:"game_indices"`
	BaseExperience int `json:"base_experience"`
	Types          []struct {
		Slot int `json:"slot"`
		Type struct {
			URL  string `json:"url"`
			Name string `json:"name"`
		} `json:"type"`
	} `json:"types"`
}

type pberries struct {
	NaturalGiftPower int `json:"natural_gift_power"`
	Flavors          []struct {
		Flavor struct {
			URL  string `json:"url"`
			Name string `json:"name"`
		} `json:"flavor"`
		Potency int `json:"potency"`
	} `json:"flavors"`
	NaturalGiftType struct {
		URL  string `json:"url"`
		Name string `json:"name"`
	} `json:"natural_gift_type"`
	Name        string `json:"name"`
	MaxHarvest  int    `json:"max_harvest"`
	SoilDryness int    `json:"soil_dryness"`
	Smoothness  int    `json:"smoothness"`
	Item        struct {
		URL  string `json:"url"`
		Name string `json:"name"`
	} `json:"item"`
	Firmness struct {
		URL  string `json:"url"`
		Name string `json:"name"`
	} `json:"firmness"`
	GrowthTime int `json:"growth_time"`
	ID         int `json:"id"`
	Size       int `json:"size"`
}

type pgeneration struct {
	Abilities     []interface{} `json:"abilities"`
	Name          string        `json:"name"`
	VersionGroups []struct {
		URL  string `json:"url"`
		Name string `json:"name"`
	} `json:"version_groups"`
	ID    int `json:"id"`
	Names []struct {
		Name     string `json:"name"`
		Language struct {
			URL  string `json:"url"`
			Name string `json:"name"`
		} `json:"language"`
	} `json:"names"`
	PokemonSpecies []struct {
		URL  string `json:"url"`
		Name string `json:"name"`
	} `json:"pokemon_species"`
	Moves []struct {
		URL  string `json:"url"`
		Name string `json:"name"`
	} `json:"moves"`
	MainRegion struct {
		URL  string `json:"url"`
		Name string `json:"name"`
	} `json:"main_region"`
	Types []struct {
		URL  string `json:"url"`
		Name string `json:"name"`
	} `json:"types"`
}

type pitem struct {
	Category struct {
		URL  string `json:"url"`
		Name string `json:"name"`
	} `json:"category"`
	Name          string      `json:"name"`
	FlingEffect   interface{} `json:"fling_effect"`
	EffectEntries []struct {
		ShortEffect string `json:"short_effect"`
		Effect      string `json:"effect"`
		Language    struct {
			URL  string `json:"url"`
			Name string `json:"name"`
		} `json:"language"`
	} `json:"effect_entries"`
	HeldByPokemon []interface{} `json:"held_by_pokemon"`
	Sprites       struct {
		Default string `json:"default"`
	} `json:"sprites"`
	GameIndices []struct {
		Generation struct {
			URL  string `json:"url"`
			Name string `json:"name"`
		} `json:"generation"`
		GameIndex int `json:"game_index"`
	} `json:"game_indices"`
	BabyTriggerFor interface{} `json:"baby_trigger_for"`
	Cost           int         `json:"cost"`
	Names          []struct {
		Name     string `json:"name"`
		Language struct {
			URL  string `json:"url"`
			Name string `json:"name"`
		} `json:"language"`
	} `json:"names"`
	Attributes        []interface{} `json:"attributes"`
	FlavorTextEntries []struct {
		Text         string `json:"text"`
		VersionGroup struct {
			URL  string `json:"url"`
			Name string `json:"name"`
		} `json:"version_group"`
		Language struct {
			URL  string `json:"url"`
			Name string `json:"name"`
		} `json:"language"`
	} `json:"flavor_text_entries"`
	ID         int           `json:"id"`
	Machines   []interface{} `json:"machines"`
	FlingPower int           `json:"fling_power"`
}

//PokemonInit begin search for pokemon related items
func PokemonInit(uncapmessage []string, message []string, s *discordgo.Session, m *discordgo.MessageCreate) {
	switch uncapmessage[1] {
	case "pokemon":
		searchvar := Pokemon{}
		helpers.GetURLFile("json", "http://pokeapi.co/api/v2/pokemon/"+message[2]+"/", &searchvar)
		//get pokemon versions and put into array
		var pversions []string
		for _, v := range searchvar.GameIndices {
			pversions = append(pversions, v.Version.Name)
		}

		var ptypes []string
		for _, v := range searchvar.Types {
			ptypes = append(ptypes, v.Type.Name)
		}

		var pabilites []string
		for _, v := range searchvar.Abilities {
			pabilites = append(pabilites, v.Ability.Name)
		}

		var pmoves []string
		for _, v := range searchvar.Moves {
			pmoves = append(pmoves, v.Move.Name)
		}

		//create message
		embed := structures.NewEmbed().
			SetTitle(searchvar.Name).
			AddField("ID:", helpers.Convertint(searchvar.ID)).
			AddField("Abilities:", helpers.Arraytostring(pabilites)).
			AddField("Available Moves:", helpers.Arraytostring(pmoves)).
			AddField("Types:", helpers.Arraytostring(ptypes)).
			AddField("Pokemon Versions:", helpers.Arraytostring(pversions)).
			SetImage(searchvar.Sprites.FrontDefault + "?size=2048").
			SetThumbnail(searchvar.Sprites.BackDefault + "?size=2048").
			SetColor(0xee1515).MessageEmbed
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
	case "berry":
		searchvar := new(pberries)
		searchvar1 := new(pitem)
		helpers.GetURLFile("json", "http://pokeapi.co/api/v2/berry/"+message[2]+"/", &searchvar)
		helpers.GetURLFile("json", "http://pokeapi.co/api/v2/item/"+searchvar.Item.Name+"/", &searchvar1)
		var pbflavors []string
		for _, v := range searchvar.Flavors {
			pbflavors = append(pbflavors, v.Flavor.Name)
		}

		embed := structures.NewEmbed().
			SetTitle(searchvar.Name).
			SetDescription(searchvar1.EffectEntries[0].Effect).
			AddField("ID", helpers.Convertint(searchvar.ID)).
			AddField("Available Flavours:", helpers.Arraytostring(pbflavors)).
			AddField("Growth Time each Cycle:", helpers.Convertint(searchvar.GrowthTime)).
			AddField("Number of Growth Cycles:", helpers.Convertint(searchvar.MaxHarvest)).
			SetThumbnail(searchvar1.Sprites.Default + "?size=1048").
			SetColor(0xee1515).MessageEmbed
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
	case "generation":
		searchvar := new(pgeneration)
		helpers.GetURLFile("json", "http://pokeapi.co/api/v2/generation/"+message[2]+"/", &searchvar)
		embed := structures.NewEmbed().
			SetTitle(searchvar.Name).
			AddField("ID", helpers.Convertint(searchvar.ID)).
			SetColor(0x00ff00).MessageEmbed
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
	case "help":
		embed := structures.NewEmbed().
			SetTitle("Pokemon Commands").
			AddField("Pokemon", "pokesearch pokemon [id or Name]").
			AddField("Berry", "pokesearch berry [id or Name]").
			AddField("Generation", "pokesearch generation [id or Name]").
			SetColor(0xee1515).MessageEmbed
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
	}
}
