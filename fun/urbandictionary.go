package fun

import (
	"fmt"
	"time"

	"../helpers"
)

//Urban structure
type Urban struct {
	List []struct {
		Definition  string        `json:"definition"`
		Permalink   string        `json:"permalink"`
		ThumbsUp    int           `json:"thumbs_up"`
		SoundUrls   []interface{} `json:"sound_urls"`
		Author      string        `json:"author"`
		Word        string        `json:"word"`
		Defid       int           `json:"defid"`
		CurrentVote string        `json:"current_vote"`
		WrittenOn   time.Time     `json:"written_on"`
		Example     string        `json:"example"`
		ThumbsDown  int           `json:"thumbs_down"`
	} `json:"list"`
}

//UrbandictionaryGet get urban definition item from message
func UrbandictionaryGet(s string) *Urban {
	searchvar := new(Urban)
	helpers.GetURLFile("json", fmt.Sprintf("https://api.urbandictionary.com/v0/define?term={%s}", s), searchvar)
	return searchvar
}
