# Libraries

[Discord Go](https://github.com/bwmarrin/discordgo)

[ffmpeg](https://www.ffmpeg.org/)

[jonas747 dca](https://github.com/jonas747/dca)

[rylio ytdl](https://github.com/rylio/ytdl)

# Hosting

Need to have git installed

1. go build

2. ./Discord_Bot -t YOUR_BOT_TOKEN

# Common Development commands

go build

    To build the code into a exectutable

Discord_Bot -t [bot token]

    To run the bot

# Commands

ping

    Pong!

pong

    Ping!

I'm a Whale

    He's a Whale!

bot help

    Send all available commands to channel

## Music

(Currently only YouTube)

music [url]

    Plays audio of the youtube video to the voice channel you are in

music list

    show list of song in queue

music shuffle

    shuffle list

## Pokémon

https://pokeapi.co/

pokemon [id or name]

    details of pokemon

berry [id or name]

    Details of berry

generation [id or name]

    Details of games in games in that generation

## BDO (Black Desert Online)

bdo map

    Map of Black Desert Online

bdo whale

    Whale Map for Black Desert Online

bdo analytics

    Analytics on Black Desert Online

## Cryptocurrency

https://coinbin.org/BTC

cryptocurrency [coin name]

    Get info on coin

# Planned

## My Anime List

https://myanimelist.net/modules.php?go=api

    Search

Possible

- add to users list
- remove to users list
- Associate their list to their list

## Animals

Get random images of animals

## Sounds

Play sounds (mostly meme sounds)

oof

    Play roblox oof sound

gg

    Play gg sound

nicememe

    Play a dude saying nice meme sound

scare

    Play a scary sound to scare the people
